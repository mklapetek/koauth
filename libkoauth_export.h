#ifndef LIBKOAUTH_EXPORT_H
#define LIBKOAUTH_EXPORT_H

/* needed for KDE_EXPORT and KDE_IMPORT macros */
#include <kdemacros.h>

#ifndef LIBKOAUTH_EXPORT
# if defined(MAKE_LIBKOAUTH_LIB)
/* We are building this library */
#  define LIBKOAUTH_EXPORT KDE_EXPORT
# else
/* We are using this library */
#  define LIBKOAUTH_EXPORT KDE_IMPORT
# endif
#endif

#endif